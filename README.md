ics-ans-role-nfs-server
=======================

Ansible role to setup a NFS server.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.10.1

Role Variables
--------------

```
nfs_server_exports: []
```

You should override the `nfs_server_exports` variable to export the directories you want.
The directories are NOT created by default. You have to set the "create" variable to "true"
if you want to create them (default to false). The group is set to root by default in this case.
You can pass the optional "group" variable to specify a different group.

Here is an example:

```
nfs_server_exports:
  - path: /export/shared
    options: "*(async,rw,insecure,crossmnt)"
    create: true
    group: nfsnobody
  - path: /export/epics
    options: "*(async,rw,insecure,crossmnt)"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nfs-server
```

License
-------

BSD 2-clause
